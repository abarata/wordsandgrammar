var Fluxxor = require('fluxxor');
var Nedb = require('nedb');

module.exports = Fluxxor.createStore({

  initialize: function(){
    this.db = new Nedb({ filename: words.getHomeDir()+'/.words/groups.json', autoload: true });
    this.log = {
      error: false,
      message: ''
    }
    this.menuGroup = [];
    this.group = {
      groupName: 'loading...',
      columns: ['loading...']
    };
    this.groups = [];
    this.bindActions(
      'CREATE_GROUP',this.onCreateGroup,
      'GET_ERROR',this.getError,
      'GET_GROUPS_MENU',this.onGetGroupMenuByUser,
      'GET_GROUP',this.onGetGroupByUser,
      'DELETE_GROUPS',this.onDeleteGroup
    )
  },
  getError: function(){
    return this.log;
  },

  // Obtiene todos los grupos del usuario y los guarda
  getDbGroupsByIdUser: function(idUser){
    var store = this;
    var promise = new Promise(function(resolve,reject){
      store.db.find({idUser: idUser},function(err,docs){
        store.groups = docs;
        resolve(store.groups);
      });
    });
    return promise;
  },

  // Devuelte todos los grupos del usuario
  getAllGroups: function(idUser){
    return this.groups;
  },

  getGroupByIndex: function(index){
    return this.groups[index];
  },

  onDeleteGroup: function(index){
    var store = this;


    store.db.remove({_id: store.groups[index]._id},{},function(err,num){
      if(index > 0){
        store.groups.splice(index,1);
        words.goTo('viewGroups',index-1);
      }else{
        store.groups.splice(index,1);
        words.goTo('index');
      }
      store.emit('change');
      words.toast('Group deleted','danger');
    });
  },















  onCreateGroup: function(group){
    var store = this;

    if(group[0].value){
      //var db = nestore.find({idUser: idUser},function(err,docs){
      var info = {
        name: '',
        columns: []
      };
      info.name = group[0].value
      group.map(function(value,index){
        if(index != 0){
          info.columns.push(value.value);
        }
        //info[value.name] = value.value;
      });
      info['idUser'] = words.getIdUser();
      store.db.insert(info,function(err,newDoc){
        if(!newDoc){
          this.log.error = true;
          this.log.message = 'error';
        }else{
          // Preparo la nueva informacion para mostrarla en el menu
          /*store.menuGroup.push({
            text: newDoc.name,
            onClick: function(){
              words.goTo('viewGroups',newDoc._id);
              words.getFlux().actions.getGroupByIdGroup(newDoc._id);
            }
          });*/
          store.groups.push(newDoc);
          store.emit('change');
          words.toast('Group added','success');
        }
      })
    }else{
      this.log.message = 'name is required';
      this.log.error = true;
      this.emit('change');
    }
  },

  onGetGroupByUser: function(data){
    //var db = new Nedb({ filename: words.getHomeDir()+'/.words/groups.json', autoload: true });
    var store = this;


    store.db.find({idUser: data.idUser,_id: data.idGroup},function(err,docs){
      //store.formatGroups(docs);
      store.group = docs[0];
      store.emit('change');
    });
  },
  onGetGroupMenuByUser: function(id){
    //var db = new Nedb({ filename: words.getHomeDir()+'/.words/groups.json', autoload: true });
    var store = this;
    store.db.find({idUser: id},function(err,docs){
      store.menuGroup.push({
        text: 'create a new group',
        onClick:function(){words.goTo('createGroup')},
        className: 'red-text text-darken-4'
      });
      docs.map(function(value){
        store.menuGroup.push({
          text: value.name,
          onClick: function(){
            words.goTo('viewGroups',value._id);
            words.getFlux().actions.getGroupByIdGroup(value._id);
          }
        });
      })
      store.emit('change');
    });
    /*var promise = new Promise(function(resolve,reject){
      db.find({idUser: id},function(err,docs){
        resolve(docs);
      });
    });*/
    //return promise;
    /*promise.then(function(groups){
      this.groups = groups;

    }.bind(this))*/
  },
  getGroups: function(){
    return this.menuGroup;
  },
  getGroup: function(){
    return this.group;
  },
  formatGroups: function(groups){
    groups.map(function(){

    })
    store.emit('change');
  }
});
