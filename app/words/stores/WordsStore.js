var Fluxxor = require('fluxxor');
var Nedb = require('nedb');

module.exports = Fluxxor.createStore({

  initialize: function(){
    this.db = new Nedb({ filename: words.getHomeDir()+'/.words/words.json', autoload: true });
    this.words = [];
    this.bindActions(
      'INSERT_WORDS',this.onCreateWord,
      'DELETE_WORD',this.onDeleteWord,
      'DELETE_ALL_WORDS_GROUP',this.onDeleteAllWordsByGroup,
      'DELETE_TAG_WORD',this.onDeleteTagWord,
      'FILTER_WORDS_BY_TAGS',this.onFilterWordsByTags
    )
  },
  /*getError: function(){
    return this.log;
  },*/

  onCreateWord: function(data){
    var store = this;
    // Es === porque el indexUpdate puede ser 0, por lo tanto falso
    // Inserto una nueva palabra
    if(data.indexUpdate === false){
      store.db.insert({words: data.words,idGroup: data.idGroup,tags: data.tags, date: new Date},function(err,word){

        store.words.push(word);
        store.emit('change');
        words.toast('New word added','success');
      });
    }else{
      // Actualizo la informacion de las palabras
      store.db.update({
        _id: store.words[data.indexUpdate]._id}
        ,{words: data.words, idGroup: data.idGroup,tags: data.tags, dateUpdate: new Date}
        ,function(err,word){

          //store.words.push(word);
          store.words[data.indexUpdate].words = data.words;
          store.words[data.indexUpdate].tags = data.tags;
          store.emit('change');
          words.toast('Word updated','success');
        }
      );
    }
  },
  onDeleteWord: function(index){
    var store = this;
    store.db.remove({_id: store.words[index]._id},function(err,num){

      store.words.splice(index,1);
      store.emit('change');
      words.toast('Word deleted','danger');
    });
  },

  onDeleteTagWord: function(data){
    console.log('Estas son las tags a filtrar para borrar');
    console.log(data);
    var store = this;
    store.db.update({tags: data.tag},{ $pull: { tags: data.tag } }, {multi: true},function(err,num){
      store.db.find({idGroup: data.idGroup},function(err,words){
        store.words = words;
        store.emit('change');
        words.toast('Word deleted','danger');
      })
    })
  },

  onDeleteAllWordsByGroup: function(idGroup){
    var store = this;


    store.db.remove({idGroup: idGroup},{ multi: true },function(err,num){
      //words.toast('All words deleted','danger');
      //store.words.splice(index,1);
      //store.emit('change');
    });
  },

  // Obtiene todas las palabras del grupo y los guarda
  getDbWordsByIdGroup: function(idGroup){
    var store = this;
    var promise = new Promise(function(resolve,reject){
      store.db.find({idGroup: idGroup},function(err,words){
        store.words = words;

        resolve(store.words);
      });
    });
    return promise;
  },

  onFilterWordsByTags: function(data){
    var store = this;
    console.log('va filtrar');
    if(data.tags != null){
      store.db.find({tags: { $in: data.tags },idGroup: data.idGroup},function(err,words){
        console.log('las palabras filtradas');
        console.log(words);
        store.words = words;
        store.emit('change');
      });
    }else{
      store.db.find({idGroup: data.idGroup},function(err,words){
        store.words = words;
        store.emit('change');
      });
    }
  },

  // Devuelte todos los grupos del usuario
  getAllWords: function(idUser){
    return this.words;
  },

  getWordByIndex: function(index){
    return this.words[index];
  },
});
