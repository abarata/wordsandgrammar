var Fluxxor = require('fluxxor');
var Nedb = require('nedb');

module.exports = Fluxxor.createStore({

  initialize: function(){
    this.menu = {}
    this.bindActions(
      'MENU_INFO',this.onGetMenuInfo
    )
  },

  onGetMenuInfo: function(idUser){
    this.waitFor(['GroupStore'],function(groupStore){

      //organizacion del menu
      /*[
        {
          header: {
            title:
          },
          body: [
            {text: 'el texto',onClick:'la accion',className:'las clases'}
          ]
        }
      ]*/
      var items = [];

      groupStore.menu.groups.map(function(value,index){
        items.push({
          text: value.name,
          onClick: function(){words.log('onClick group','ir al grupo')}
        });
      });
      this.menu['groups'] = items;
      //this.menu = groups.menu;
      this.emit('change');
    })
  },

  getInfo: function(){
    return this.menu;
  }

});
