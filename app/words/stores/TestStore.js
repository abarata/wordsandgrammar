var Fluxxor = require('fluxxor');
var Nedb = require('nedb');

module.exports = Fluxxor.createStore({

  initialize: function(){
    this.db = new Nedb({ filename: words.getHomeDir()+'/.words/test.json', autoload: true });
    this.data = '';
    this.status = true;// true se ha modificado, false no se ha modificado
    this.bindActions(
      'INSERT_TEST',this.onInsert,
      'GET_TEST',this.onGetDbData
    )
  },
  onInsert: function(data){
    //this.db.insert({info: data});
    console.log('2. esta insertando el test');
    this.data = data;
    console.log('3. comprobar dato: '+this.data);
    this.emit('change');
  },
  onGetDbData: function(){
    store = this;
    var promise = new Promise(function(resolve,reject){
      store.db.findAll({},function(err,docs){
        resolve(docs);
      })
      //console.log('va a lanzar el resolve: '+store.data);
      //store.status = false;
      //resolve(store.data);
    });
    return promise;
  },
  getData: function(){
    //store.status = false;
    return this.data;
  },
  getStatus: function(){
    return this.status;
  },
  notify: function(){
    this.status = true;
    this.emit('change');
  }
});
