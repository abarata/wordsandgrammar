var Fluxxor = require('fluxxor');
var Nedb = require('nedb');

module.exports = Fluxxor.createStore({

  initialize: function(){
    this.db = new Nedb({ filename: words.getHomeDir()+'/.words/tags.json', autoload: true });
    this.tags = [];
    this.bindActions(
      //'GET_TAGS',this.onGetTagsByUser,
      'CREATE_TAG',this.onCreateTag,
      'DELETE_TAG',this.onDeleteTag,
      'DELETE_ALL_TAGS',this.onDeleteAllTagsByGroup
    )
  },

  onCreateTag: function(data){
    var store = this;
    if(!data.idGroup){
      idGroup = 'global';
    }
    store.db.insert({idGroup: data.idGroup, idUser: words.getIdUser(),tag: data.tags, date: new Date},function(err,doc){
      store.tags.push(doc);
      console.log('LAS TAGS');
      console.log(store.tags);
      store.emit('change');
      words.toast('Tag added','success');
    });
  },

  onDeleteTag: function(data){
    var store = this;
    console.log('se va a eliminar la tag: ');
    console.log(data);
    store.db.remove({_id: data.id},function(err,doc){
      store.tags.splice(data.index,1);
      store.emit('change');
      words.toast('Tag deleted','danger');
    })
  },
  onDeleteAllTagsByGroup: function(idGroup){
    var store = this;
    store.db.remove({idGroup: idGroup},function(err,doc){
      //store.tags = [];
      //store.emit('change');
      //words.toast('All tags deleted','danger');
    })
  },
  getTagsByGroup: function(idGroup){
    var store = this;
    var promise = new Promise(function(resolve,reject){
      store.db.find({idUser: words.getIdUser(),idGroup: idGroup},function(err,docs){
        store.tags = docs;
        resolve(store.tags);
      });
    });
    return promise;
  },

  getAllTags: function(){
    return this.tags;
  }

});
