var Fluxxor = require('fluxxor');
var Nedb = require('nedb');
var Bcrypt = require('bcrypt');

module.exports = Fluxxor.createStore({

  initialize: function(){
    this.user = {}
    this.login =  false;
    //this.register = false;
    this.message = '';
    this.bindActions(
      'ADD_USER',this.onAddUser,
      'MODIFIY_USER',this.onModifyUser,
      'REMOVE_USER',this.onRemoveUser,
      'GET_USER',this.onGetUser,
      'LOGIN_USER',this.onLoginUser,
      'CREATE_USER',this.onCreateUser,
      'IS_LOGIN',this.onIsLogin
    )
  },
  onAddUser: function(){

    this.user.text = 'Un nuevo usuario';
    this.emit('change');

  },

  onModifyUser: function(){

  },

  onRemoveUser: function(){

  },

  onGetUser: function(){
    return this.user;
  },

  onLoginUser: function(user){
    //./app/words/db/users'
    var db = new Nedb({ filename: words.getHomeDir()+'/.words/users.json', autoload: true });
    //

    db.find({username: user.username},function(err,docs){

      if(docs.length != 0){

        Bcrypt.compare(user.password,docs[0].password,function(err,res){
          if(res){

            this.login = true;

            words.setId(docs[0]._id);
            words.goTo('index');
            //this.emit('change');
          }else{
            this.message = 'the password is incorrect';
            this.emit('change');
          }
        }.bind(this));
      }else{
        this.message = 'the username is incorrect';
        this.emit('change');
      }
    }.bind(this));
  },

  onCreateUser: function(user){
    var db = new Nedb({ filename: words.getHomeDir()+'/.words/users.json', autoload: true });
    db.find({username: user.username},function(err,docs){

      if(docs.length == 0){
        Bcrypt.genSalt(10,function(err,salt){
          Bcrypt.hash(user.password,salt,function(err,hash){
            user.password = hash;
            db.insert(user,function(err,newDoc){

              if(newDoc){
                //this.register =  true;
                this.login = true;
                this.message = 'the user is created';


                words.setId(newDoc._id);
                this.emit('change');
                words.toast('User added','success');
              }
            }.bind(this));
          }.bind(this));
        }.bind(this));
      }else{
        this.message = user.username+' is already used';
        this.emit('change');
      }
    }.bind(this));
  },

  onIsLogin: function(){

    if(!this.login){

      return this.message
    }else{

      return true;
    }
  }
});
