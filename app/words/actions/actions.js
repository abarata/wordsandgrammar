module.exports = {
  addUser: function(user){
    this.dispatch('ADD_USER',user);
  },
  isLogin: function(){
    this.dispatch('IS_LOGIN')
  },
  createUser: function(user){
    this.dispatch('CREATE_USER',user);
  },
  loginUser: function(user){

    this.dispatch('LOGIN_USER',user);
  },
  createGroup: function(group){

    this.dispatch('CREATE_GROUP',group)
  },
  getInfoMenu: function(){
    this.dispatch('GET_GROUPS_MENU',words.getIdUser());
    //this.dispatch('MENU_INFO');
  },
  /*getTags: function(){
    this.dispatch('GET_TAGS',words.getIdUser());
  },*/
  getGroupByIdGroup: function(idGroup){
    data = {
      idUser: words.getIdUser(),
      idGroup: idGroup
    }
    this.dispatch('GET_GROUP',data);
  },
  deleteGroup: function(index){
    this.dispatch('DELETE_GROUPS',index);
  },
  insertWords: function(words,tags,idGroup,indexUpdate){
    var data = {
      words: words,
      idGroup: idGroup,
      indexUpdate: indexUpdate,
      tags: tags
    }
    console.log(data);
    this.dispatch('INSERT_WORDS',data);
  },
  deteleWord: function(index){
    this.dispatch('DELETE_WORD',index);
  },
  deleteAllWordsByGroup: function(idGroup){
    this.dispatch('DELETE_ALL_WORDS_GROUP',idGroup);
  },
  insertTag: function(data){
    this.dispatch('CREATE_TAG',data);
  },
  removeTag: function(data){
    console.log('Eliminacion de la tag');
    this.dispatch('DELETE_TAG',data);
    //this.dispatch('DELETE_TAG_WORD',data);
  },
  deleteAllTagsByGroup: function(group){
    this.dispatch('DELETE_ALL_TAGS',group);
  },
  deleteTagWord: function(data){
    this.dispatch('DELETE_TAG_WORD',data);
  },
  filterWordsByTags: function(data){
    console.log('filtrando');
    this.dispatch('FILTER_WORDS_BY_TAGS',data);
  },
  testGetDbData: function(){
    this.dispatch('GET_TEST');
  },
  testInsert: function(data){
    this.dispatch('INSERT_TEST',data);
  }
}
