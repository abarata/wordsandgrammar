var React = require("react");
// No tiene mizin porque el evento se produce en el principal, y pasa la informacion
// a sus hijos
module.exports = React.createClass({
  /*propTypes: {
    type: React.PropTypes.string,
    items: React.PropTypes.arrayOf(React.PropTypes.shape({
      header: React.PropTypes.shape({
        icon: React.PropTypes.string,
        title: React.PropTypes.string
      }),
      body: React.PropTypes.arrayOf(React.PropTypes.shape({
        text: React.PropTypes.string,
        onClick: React.PropTypes.func,
        className: React.PropTypes.string,
        link: React.PropTypes.string
      }))
    }))
  },*/
  componentDidMount: function(){
    //$('.collapsible').collapsible();
  },
  getDefaultProps : function(){
    return {
      modalName: 'modal-prueba',
      complete: function(){alert('cerrando modal')},
      fixed: false,
      trigger: {
        className: 'modal-trigger waves-effect waves-light btn',
        text: 'Modal'
      },
      content: 'Un react element',
      footer: [
        {
          text: 'save',
          close: false,
          onClick: function(){}
        },
        {
          text: 'close',
          close: true,
          onClick: function(){}
        }
      ]
    }
  },
  componentDidMount: function(){
  
  },
  render: function(){
    var options = this.props;
    var trigger = '';
    var fixed = '';
    if(options.hasOwnProperty('trigger')){
      trigger = React.DOM.a({className: options.trigger.className, href: '#'+options.modalName},options.text);
    }
    if(options.fixed){
      fixed = 'modal-fixed-footer';
    }
    return (
      React.DOM.div({},
        trigger,
        React.DOM.div({id: options.modalName, className: 'modal'+' '+fixed},
          React.DOM.div({className: 'modal-content'},
            options.content
          ),
          React.DOM.div({className: 'modal-footer'},
            options.footer.map(function(value,index){
              var className = ['modal-action'];
              if(value.hasOwnProperty('close')){
                if(value.close){
                  className.push('modal-close');
                }
              }
              className.push('waves-effect waves-green btn-flat');
              return React.DOM.a({className:className.join(' '),onClick: value.onClick, key: 'button-modal-'+index},value.text)
            })
          )
        )
      )
    )
  },
  configuration: function(props){
    var options = {};
    // configuracion boton de lanzar modal
    if(props.hasOwnProperty('trigger')){
    }
  }
})
