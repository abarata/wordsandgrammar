var React = require("react");
// No tiene mizin porque el evento se produce en el principal, y pasa la informacion
// a sus hijos
module.exports = React.createClass({
  /*propTypes: {
    type: React.PropTypes.string,
    items: React.PropTypes.arrayOf(React.PropTypes.shape({
      header: React.PropTypes.shape({
        icon: React.PropTypes.string,
        title: React.PropTypes.string
      }),
      body: React.PropTypes.arrayOf(React.PropTypes.shape({
        text: React.PropTypes.string,
        onClick: React.PropTypes.func,
        className: React.PropTypes.string,
        link: React.PropTypes.string
      }))
    }))
  },*/
  /*componentDidMount: function(){
    $('.collapsible').collapsible();
  },*/
  getDefaultProps : function(){
    return {
      toggle: false,
      button:{
        color: 'red',
        icon: 'mdi-navigation-menu'
      },
      items: [
        {
          color: 'red',
          icon: 'library_add',
        },
        {
          color: 'yellow darken-1',
          icon: 'label'
        }
      ]
    }
  },
  render: function(){
    var options = this.props;
    var toggle = '';
    if(options.toggle){
      toggle = 'click-to-toggle';
    }
    return (
      React.DOM.div({className: 'fixed-action-btn horizontal'+' '+toggle},
        React.DOM.a({className: 'btn-floating btn-large'+' '+options.button.color},
          React.DOM.i({className: 'large'+' '+options.button.icon})
        ),
        React.DOM.ul({},
          this.props.items.map(function(value,index){
            //console.log(options);
            return (
              React.DOM.li({className: 'btn-floating'+' '+value.color,onClick: value.onClick, key: 'horizontal-icon-'+index},
                React.DOM.i({className: 'material-icons '+value.className},value.icon)
              )
            )
          })
        )
      )
    )
  }
})
