var React = require("react");
// No tiene mizin porque el evento se produce en el principal, y pasa la informacion
// a sus hijos
module.exports = React.createClass({
  propTypes: {
    type: React.PropTypes.string,
    items: React.PropTypes.arrayOf(React.PropTypes.shape({
      header: React.PropTypes.shape({
        icon: React.PropTypes.string,
        title: React.PropTypes.string
      }),
      body: React.PropTypes.arrayOf(React.PropTypes.shape({
        text: React.PropTypes.string,
        onClick: React.PropTypes.func,
        className: React.PropTypes.string,
        link: React.PropTypes.string
      }))
    }))
  },
  componentDidMount: function(){
    $('.collapsible').collapsible();
  },
  getDefaultProps : function(){
    return {
      type: 'accordion', //accordion || expandable
      items: [
        {
          header: {
            title: 'Primer titulo'
          },
          body: 'string o element'
        }
      ]
    }
  },
  render: function(){
    var options = this.props;

    return (
      React.DOM.ul({className: 'collapsible', 'data-collapsible': options.type},
        options.items.map(function(value,index){
          var title;
          if(value.header.hasOwnProperty('icon')){
            title = React.DOM.i({className: value.header.icon}),value.header.title;
          }else{
            title = value.header.title;
          }

          return (
            React.DOM.li({key: 'collapsible-li-'+index},
              React.DOM.div({className: 'collapsible-header'},title),
              React.DOM.div({className: 'collapsible-body'},
                React.DOM.div({className: 'collection'},
                  value.body.map(function(body,indexBody){
                    if(!body.hasOwnProperty('link')){
                      body.link = '#'
                    }
                    return React.DOM.a({
                      className: 'collection-item'+' '+body.className,
                      onClick: body.onClick,
                      key:'item'+indexBody,
                      href: body.link
                    },
                      body.text
                    )
                  })
                )
              )
            )
          )
        })
      )
    )
  }
})
