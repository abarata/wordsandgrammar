var React = require("react");
// No tiene mizin porque el evento se produce en el principal, y pasa la informacion
// a sus hijos
module.exports = React.createClass({
  getDefaultProps : function(){
    return {
      div: {
        className: 'input-field'
      },
      input: {
        text: 'text',
        id: 'text',
        type: 'text',
      },
      label: {
        htmlFor: 'text',
        text: 'text'
      }
    }
  },
  render: function(){
    var options = this.props;
    return (
      React.DOM.div(options.div,
        React.DOM.input(options.input),
        React.DOM.label(options.label,options.label.value)
      )
    )
  }
})
