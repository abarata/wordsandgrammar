var React = require("react");
// No tiene mizin porque el evento se produce en el principal, y pasa la informacion
// a sus hijos
module.exports = React.createClass({
  /*propTypes: {
    type: React.PropTypes.string,
    items: React.PropTypes.arrayOf(React.PropTypes.shape({
      header: React.PropTypes.shape({
        icon: React.PropTypes.string,
        title: React.PropTypes.string
      }),
      body: React.PropTypes.arrayOf(React.PropTypes.shape({
        text: React.PropTypes.string,
        onClick: React.PropTypes.func,
        className: React.PropTypes.string,
        link: React.PropTypes.string
      }))
    }))
  },*/
  componentDidMount: function(){
    $('.collapsible').collapsible();
  },
  getDefaultProps : function(){
    return {
      table: {
        options: {
          className: 'highlight'
        },
        head: ['titulo 1','titulo 2','titulo 3'],
        body: [
          ['informacion 1','informacion 2', 'informacion 3'],
          ['informacion 4','informacion 5', 'informacion 6']
        ],
        //buttons: false,
        buttons: {
          edit: {
            text: 'Edit',
            onClick: function(){},
          },
          delete: {
            text: 'Delete',
            onClick: function(){}
          }
        }
      }
    }
  },
  configuration: function(info){
    if(info.head){
      // preparo las cabeceras
      info.head = info.head.map(function(value,index){
        return React.DOM.th({"data-field":'id', key:'table-value-'+index},value);
      });
      // preparo la cabecera de las Tags
      info.head.push(React.DOM.th({"data-field":'id', key:'table-value-tags'},'Tags'));
      // preparo la cabecera de los botones
      if(info.buttons){
        info.head.push(React.DOM.th({"data-field":'id', key:'table-value-buttons'},'Buttons'));
      }
    }
    if(info.body){
      info.body = info.body.map(function(row,rowIndex){
        // Donde se guardan todas filas, tanto las de tags como los botones
        var td = row.map(function(column,columnIndex){
          // si es un array, significa que son las tags
          //Array.isArray(column)


          if(Array.isArray(column) && column.length > 0){

            return(
              React.DOM.td({key:'table-'+column+'-'+columnIndex},
                column.map(function(tag,tagIndex){
                  return React.DOM.div(
                    {
                      key:'div-tag-'+tagIndex,
                      className: 'chip',
                      onClick: function(){
                        $('#tags-filter').select2().val(tag).trigger('change');
                      }
                    },
                    tag);
                })
              )
            )
          }else{
            if(Array.isArray(column)){
              column = '';
            }
            return React.DOM.td({key:'table-'+column+'-'+columnIndex},column)
          }

        });

        // Colocacion botones

        if(info.buttons != null){
          td.push(
            React.DOM.td({key: 'table-buttons'},
              React.DOM.a({
                className: 'waves-effect waves-light btn',
                // Funcion provisional
                onClick: info.buttons.edit.onClick.bind(undefined,row,rowIndex)
              },info.buttons.edit.text),
              React.DOM.a({
                className: 'waves-effect waves-light btn',
                onClick: info.buttons.delete.onClick.bind(undefined,rowIndex)/*function(){console.log('Eliminar palabra: '+rowIndex)}*/
              },info.buttons.delete.text)
            )
          );
        }

        return (
          React.DOM.tr({key: 'table-row-'+rowIndex},
            td
          )
        )
      });
    }else{
      info.body = '';
    }

    return info;
  },
  render: function(){
    var options = this.configuration(this.props.table);


    return (
      React.DOM.table(options.options,
        React.DOM.thead({},
          React.DOM.tr({},
            /*options.head.map(function(value,index){
              return React.DOM.th({"data-field":'id', key:'table-value-'+index},value);
            })*/
            options.head
          )
        ),
        React.DOM.tbody({},
          options.body
        )
      )
    )
  }
})
