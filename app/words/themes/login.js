var React = require("react");
var Fluxxor = require("fluxxor");
var input = require('./../components/input');

var FluxMixin = Fluxxor.FluxMixin(React);
var StoreWatchMixin = Fluxxor.StoreWatchMixin;

module.exports = React.createClass({
  mixins: [FluxMixin,StoreWatchMixin("UserStore")],

  getStateFromFlux: function() {
    var flux = this.getFlux();
    //console.log(flux.store("UserStore").onGetUser());
    var temp = flux.store("UserStore").onIsLogin();

    if(typeof temp ==  'boolean'){
      // mover a la pagina principal

      words.goTo('index');
    }
    return {info: temp}
    //return flux.store("UserStore").onIsLogin();
  },

  submitUser: function(){
    var user  = {
      username: document.getElementById('username').value,
      password: document.getElementById('password').value
    };
    this.getFlux().actions.loginUser(user);
  },

  createUser: function(){
    var user  = {
      username: document.getElementById('username').value,
      password: document.getElementById('password').value
    };

    this.getFlux().actions.createUser(user);
  },

  render: function(){
    //var test = this.state.text;
    return (
      React.DOM.div({className: 'container'},
        React.DOM.div({className: 'row'},
          React.DOM.div({className: 'col s12 m12 l6 offset-l3'},
            React.DOM.div({className: 'card blue-grey darken-1 valign center'},
              React.DOM.span({},this.state.info),
              React.DOM.div({className: 'card-content white-text'},
                React.createElement(input,{
                  div: {
                    className: 'input-field col s12'
                  },
                  input: {
                    id: 'username',
                    type: 'text',
                    //placeholder: 'username',
                    //className: 'validate'
                  },
                  label: {
                    htmlFor: 'username',
                    value: 'Username'
                  }
                }),
                React.createElement(input,{
                  div: {
                    className: 'input-field col s12'
                  },
                  input: {
                    id: 'password',
                    type: 'password',
                    //placeholder: 'password',
                    //className: 'validate'
                  },
                  label: {
                    htmlFor: 'password',
                    value: 'Password'
                  }
                })
              ),
              React.DOM.div({className: 'card-action without-border'},
                React.DOM.a({href: '#',onClick: this.submitUser},'Login'),
                React.DOM.a({href: '#',onClick: this.createUser},'create User')
              )
            )
          )
        )
      )
    )
  }
})
