module.exports = (function(){

  var nameTemplates = ['login','main'];
  var templates = {};
  if(nameTemplates.length != 0){
    nameTemplates.forEach(function(value,index){    
      templates[value] = require('./'+value);
    })
  }
  return templates;
})();
