var React = require("react");
var Fluxxor = require("fluxxor");
var collapsible = require('./../components/collapsible');

var FluxMixin = Fluxxor.FluxMixin(React);
var StoreWatchMixin = Fluxxor.StoreWatchMixin;
var options = false;

module.exports = React.createClass({
  mixins: [FluxMixin,StoreWatchMixin("GroupStore")],
  // Modifico los datos para hacer el menu
  createMenu: function(groups){
    options = {}
    options['groups'] = [];
    options['grammar'] = [];
    options['user'] = [];
    // Pongo el boton para ir a la vista de crear un nuevo grupo
    options['groups'].push({
      text: 'Create group',
      onClick:function(){words.goTo('createGroup')},
      className: 'red-text text-darken-4'
    });
    // Pongo el resto de botones que son los grupos
    groups.map(function(value,index){
      options['groups'].push({
        text: value.name,
        onClick: function(){words.goTo('viewGroups',index);}
      });
    });
    // Configuro las opciones del usuario
    options['user'].push({
      text: 'Profile',
      onClick: function(){console.log('vamos al profile');}
    });

    options['user'].push({
      text: 'Dashboard',
      onClick: function(){console.log('vamos al dashboard');}
    });

    // configuro las opciones de la gramatica
    options['grammar'].push({
      text: 'Create grammar group',
      onClick: function(){console.log('ir a crear un nuevo grupo');},
      className: 'red-text text-darken-4'
    })
  },
  getStateFromFlux: function() {

    var flux = this.getFlux();
    if(options){
      console.log('Un nuevo grupo');
      this.createMenu(flux.store('GroupStore').getAllGroups());
    }else{
      console.log('Todos los grupos');
      this.createMenu(this.props.data);
    }
    /*var groups = flux.store('GroupStore').getGroups();
    var tags = [];*/

    //return {groups: groups, tags: tags}
    return {}
  },
  componentWillUnmount: function(){
    console.log('ELIMINACIONNNNNNNNNNNN');
    options = false;
  },
  /*componentDidMount: function(){
    this.getFlux().actions.getInfoMenu();
  },*/
  render: function(){
    return (
      React.DOM.div({className: 'row'},
        React.DOM.div({className: 'col l2'},
          React.createElement(collapsible,{
            type: 'accordion',
            items: [
              {
                header: {
                  title: 'User',
                },
                body: options.user
              },
              {
                header: {
                  title: 'Words'
                },
                body: options.groups
                /*[
                  {text: 'create a new group',onClick:function(){words.goTo('createGroup')},className: 'red-text text-darken-4'},
                  {text: 'Prueba dos',onClick:function(){words.goTo('index')}},
                  {text: 'Prueba tres',onClick:function(){words.goTo('index')}}
                ]*/
              },
              {
                header: {
                  title: 'Grammar'
                },
                body: options.grammar
              }
            ]
          })
        ),
        React.DOM.div({className: 'col l10'},
          React.createElement(this.props.view,{flux: words.getFlux(),data: this.props.data})
        )
      )
    );
  }
})
