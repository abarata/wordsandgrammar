var Fluxxor = require('fluxxor');
var UserStore = require('./stores/UserStore');
var GroupStore = require('./stores/GroupStore');
var TagStore = require('./stores/TagStore');
var MainStore = require('./stores/MainStore');
var TestStore = require('./stores/TestStore');
var WordsStore = require('./stores/WordsStore');
var viewsController = require('./views/viewController');
var markdown = require('markdown-it');
var os = require('os');

module.exports = (function(){
  var logCollapsed = true;
  var flux;
  var md = new markdown();
  var idUser = 0;
  var homeDir = '';
  var test = this;
  var init = function(){
    var actions = require('./actions/actions');
    homeDir = os.homedir();
    var stores = {
      UserStore: new UserStore(),
      GroupStore: new GroupStore(),
      MainStore: new MainStore(),
      TagStore: new TagStore(),
      TestStore: new TestStore(),
      WordsStore: new WordsStore()
    }
    flux = new Fluxxor.Flux(stores,actions);
  }

  return{
    cua: this,
    getFlux: function(){
      if(!flux){
        init();
      }
      return flux;
    },
    toHtml: function(text){
      return md.render(text);
    },
    isGuest: function(){
      if(idUser == 0){
        return true;
      }else{
        return false;
      }
    },
    setId: function(id){
      idUser = id;
    },
    getIdUser: function(){
      return idUser;
    },
    goTo: function(view,data){
      viewsController[view](data);
    },
    setLogCollapsed: function(toggle){
      logCollapsed = toggle;
    },
    getHomeDir: function(){
      return homeDir;
    },
    /**
    * title: titulo
    * message: el mensaje
    * argumento 2: el tipo de mensaje
    * argumento 3: si se muestra el contenido del mensaje o
      solo el titulo (true: solo se muestra el titulo)
    */
    log: function(title,message){
      // find all the console.log
      //console\.log\('?[a-z, ]+'?\)\;
      //words.log(.+);
      var type = 'log';
      var collapsed = true;
      console.log(typeof arguments[2]);
      if(typeof arguments[2] === 'boolean'){
        collapsed = arguments[2];
      }
      if(arguments.length == 4){
        collapsed = arguments[3];
      }
      //console.info(collapsed,logCollapsed);
      if(collapsed && logCollapsed){
        console.groupCollapsed(title.toUpperCase());
      }else{
        console.group(title.toUpperCase());
      }

      switch(arguments[2]){
        case 'info':
          type = 'info';
          break;
        case 'error':
          type = 'error';
          break;
        case 'warn':
          type = 'warn';
          break;
        default:
          if(arguments[2] && typeof arguments[2] !== 'boolean'){
            console.debug('EL TYPE NO ES CORRECTO');
          }
      }
      if(typeof message === 'object'){
        type = 'dir';
      }
      console[type](message);
      console.groupEnd();
    },
    toast: function(message,type){
      Materialize.toast(message,3000,type+'-toast');
    },
    test: function(prueba){
      console.log(this);
      console.log(test);
    }
  }

})();
