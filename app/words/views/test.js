var React = require("react");
var Fluxxor = require("fluxxor");

var FluxMixin = Fluxxor.FluxMixin(React);
var StoreWatchMixin = Fluxxor.StoreWatchMixin;
// Donde se guardaran los datos y se modificaran del props
var options = {}

// Recibe los primeros datos mediante el props
// Se pasan a options y se modificaran en options
// Ya no recibira ningun props mas, las modificaciones del options
// se haran en getStateFromFlux.
// Se recibiran porque se lanza un emit('change') en la store
module.exports = React.createClass({
  mixins: [FluxMixin,StoreWatchMixin("TestStore")],
  getStateFromFlux: function() {
    var flux = this.getFlux();
    if(options){
      options.data = flux.store('TestStore').getDataOtro();
    }else{
      options = this.props;
    }
    //var data = flux.store('TestStore').getDataOtro();
    return {}
  },
  /*componentWillUpdate: function(newProps,newState){

  },*/
  render: function(){
    console.log('DATAAAAAAA:');
    console.log(options);
    return (
      React.DOM.div({},'LA INFORMACION ES: '+options.data,
        React.DOM.a({onClick: function(){
          console.log('1. Envia insertar test');
          words.getFlux().actions.testInsert('pepe');
          //words.goTo('test');
        }},'insert')
      )
    )
  }
});
