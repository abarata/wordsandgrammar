var React = require('react');
var ReactDOM = require('react-dom');
var allViews = require('./allViews');
var allTemplates = require('./../themes/allTemplates');
var dada = require('./groups/viewGroups');

module.exports = {
  index: function(){
    var controller = this;
    if(words.isGuest()){

      this.render('','login');
    }else{

      var promise = words.getFlux().store('GroupStore').getDbGroupsByIdUser(words.getIdUser());
      promise.then(function(groups){
        controller.render('index','main',groups);
      })
      //this.render('index','main');
    }
  },
  tags: function(){

    this.render('tags','main');
  },
  createGroup: function(){
    this.render('createGroups','main');
  },
  viewGroups: function(id){
    var controller = this;
    var group = words.getFlux().store('GroupStore').getGroupByIndex(id);
    var promiseWords = words.getFlux().store('WordsStore').getDbWordsByIdGroup(group._id);
    var promiseTags = words.getFlux().store('TagStore').getTagsByGroup(group._id);
    promiseWords.then(function(words){
      promiseTags.then(function(tags){
        var data = {
          group: group,
          words: words,
          indexGroup: id,
          tags: tags
        }
        controller.render('viewGroups','main',data);
      })
    });
  },

  render: function(view,template,data){
    var template;
    if(template){
      template = allTemplates[template];
    }else{
      template = 'main';
    }
    //console.log('Vista');
    //console.log(allViews[view]);
    ReactDOM.render(React.createElement(template,{flux: words.getFlux(),view: allViews[view],data: data}),document.getElementById('app'));
  },
  test: function(){
    var data = words.getFlux().store('TestStore').onGetDbData();
    var controller = this;
    data.then(function(value){
      console.log('enviar para renderizar: '+value);
      controller.render('test','main',value);
    })
  }
}
