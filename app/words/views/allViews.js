module.exports = (function(){

  var routs = [
    'tags/tags',
    'groups/createGroups',
    'index',
    'groups/viewGroups',
    'test'
  ];
  var names = [
    'tags',
    'createGroups',
    'index',
    'viewGroups',
    'test'
  ]
  var views = {};
  if(routs.length != 0){
    routs.forEach(function(value,index){

      views[names[index]] = require('./'+value);
    })
  }
  return views;
})();
