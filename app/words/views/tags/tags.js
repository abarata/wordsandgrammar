var React = require("react");
var Fluxxor = require("fluxxor");
var table = require('./../../components/table');

var FluxMixin = Fluxxor.FluxMixin(React);
var StoreWatchMixin = Fluxxor.StoreWatchMixin;

module.exports = React.createClass({
  mixins: [FluxMixin,StoreWatchMixin("TagStore")],
  getStateFromFlux: function() {
    var flux = this.getFlux();
    var tags = flux.store('TagStore').getTags();
    words.log('TAG',tags);
    return {columns: 1, tags: tags}
  },
  componentDidMount: function(){
    this.getFlux().actions.getTags();
  },
  render: function(){
    var options = this.props;
    return (
      React.DOM.div({},
        React.createElement(table,{
          table: {
            head: [
              'Tag',
              'Group',
              'Actions'
            ],
            body: [
              this.state.tags
            ]
          }
        })
      )
    )
  }
});
