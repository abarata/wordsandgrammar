var React = require("react");
var Fluxxor = require("fluxxor");

var FluxMixin = Fluxxor.FluxMixin(React);
var StoreWatchMixin = Fluxxor.StoreWatchMixin;

module.exports = React.createClass({
  mixins: [FluxMixin,StoreWatchMixin("GroupStore")],
  getStateFromFlux: function() {
    var flux = this.getFlux();
    var log = flux.store('GroupStore').getError();
    var message = '';
    if(log.error){
      message = log.message;
    }else{
      //Ir al grupo creado
    }
    return {columns: 1, message: message}
  },
  /*getInitialState: function(){
    return {columns: 1}
  },*/
  render: function(){
    var options = this.props;
    var columns = [];
    //
    $('form').submit(function(event){

      event.preventDefault();

    });
    for(var i = 1; i<= this.state.columns; i++){
        var column = React.DOM.div({className: 'input-field', key:'column'+i},
          React.DOM.input({className: 'validate',id:'column'+i, type:'text',name: 'column'+i}),
          React.DOM.label({htmlFor: 'column'+i},'Column')
        )
        columns.push(column);
    }
    return (
      React.DOM.form({className: 'col s12 m12 l8 offset-l2'},
        React.DOM.h1({className:'center-align'},'Create a group'),
        React.DOM.span({className:'note'},this.state.message),
        React.DOM.div({className: 'input-field'},
          React.DOM.input({className: 'validate',id:'name', type:'text', name:'name'}),
          React.DOM.label({htmlFor: 'name'},'Name')
        ),
        columns,
        React.DOM.a({
          className:'waves-effect waves-light btn',
          onClick: function(){
            this.setState({columns: this.state.columns + 1})
          }.bind(this)},
          'New Column'
        ),
        React.DOM.a({
          className:'waves-effect waves-light btn',
          onClick: function(){


            this.getFlux().actions.createGroup($('form').serializeArray());
            $('form')[0].reset();
          }.bind(this)
        },
          'Save'
        )
      )
    )
  }
})
