var React = require("react");
var Fluxxor = require("fluxxor");
var table = require('./../../components/table');
var horizontalFab = require('./../../components/horizontalFab');
var modal = require('./../../components/modal');
var input = require('./../../components/input');

var FluxMixin = Fluxxor.FluxMixin(React);
var StoreWatchMixin = Fluxxor.StoreWatchMixin;
var options = false;

module.exports = React.createClass({
  mixins: [FluxMixin,StoreWatchMixin("GroupStore",'WordsStore','TagStore')],
  getStateFromFlux: function() {
    var flux = this.getFlux();
    if(!options){

      options = this.props.data;

    }else{
      options['group'] = flux.store('GroupStore').getGroupByIndex(options.indexGroup);
      options['words'] = flux.store('WordsStore').getAllWords();
      options['tags'] = flux.store('TagStore').getAllTags();

    }
    this.configuration();


    return {}
  },
  // como getStateFromFlux solo se ejecuta cuando: se hace un emit('change'),
  // se hace un setState() o la primera vez en montarse el componente y
  // no cuando se mandan nuevos props, se debe de usar
  // esta funcion para poner los nuevos datos que vienen del props
  componentWillReceiveProps: function(newProps){

    options = newProps.data;
    this.configuration();

  },
  // la variable options no se elimina, entonces se tiene que volver a un
  // estado inicial, para cuando se monte de nuevo este componente inicie los
  // datos en getStateFromFlux al principio
  componentWillUnmount: function(){

    options = false;
  },
  
  // Los compoentes que se tienen que incializar al principio
  componentDidMount: function(){
    //$('select').material_select();
    $('select').select2({
      //theme: "classic"
    });
    $('#tags-filter').on('change',function(){
      console.log('Ha cambiado');
      console.log($('#tags-filter').select2().val());
      words.getFlux().actions.filterWordsByTags({
        tags: $('#tags-filter').select2().val(),
        idGroup: options['group']._id
      });
    });
  },
  // Configuracion de los datos, cuando se reciben nuevos props o
  // se actualiza mediante emit('change')
  // Todos los datos vuelve de nuevo y forma el options otra vez
  configuration: function(info){
    console.log('ES LA INFO DE VIEWGROUP');
    console.log(options);
    options['wordsTable'] = [];
    // Cuando la configuracion se hace y para configurar los valores
    // iniciales y no actualizar, crea el options['update']
    if(!options.hasOwnProperty('update')){
      options['update'] = false;
    }
    //options['update'] = false;
    if(options['words']){
      options['words'].map(function(word,index){
        var temp = Array.from(word.words);
        temp.push(word.tags);
        options['wordsTable'].push(temp);
      })
    }
    // Configuracion de las opciones del modal
    // al abrir el modal
    options['configModal'] = {
        dismissible: false,
        ready: function(){console.log('el ready funciona')},
        complete: function(viewOptions){

          viewOptions['update'] = false;
          $('#form-words')[0].reset();
          $('#tags-select-modal-prueba').select2().val('');
        }.bind(undefined,options)
      }
  },
  insertWord: function(){

    var values = [];
    var tags;
    options['group'].columns.map(function(value){
      values.push($('#'+value).val());
    });
    tags = $('#tags-select-modal-prueba').val();

    // Enviar los documentos
    words.getFlux().actions.insertWords(values,tags,options['group']._id,options['update']);
    // Reiniciar los valores
    if(options['update'] === false){
      $('#form-words')[0].reset();
      $('#tags-select-modal-prueba').select2().val(null).trigger('change');
    }
  },
  insertTag: function(){
    var data = {}
    data.tags = $('#input-tag').val();
    data.idGroup = options['group']._id;
    words.getFlux().actions.insertTag(data);
    $('#input-tag').val('');
  },
  /*componentDidMount: function(){
    this.getFlux().actions.getGroupByIdGroup(this.props.data);
  },*/
  render: function(){
    //var options = this.props;
    return (
      React.DOM.div({},
        React.DOM.h1({className:'center-align'},options['group'].name),
        // Formulario de filtros
        React.DOM.div({},
          React.DOM.label({},'Tags filter'),
          React.DOM.select({
            multiple: true,
            id: 'tags-filter',
            className:'browser-default'
          },
            options['tags'].map(function(tag,indexTag){
              return React.DOM.option({key: 'tag-select-filter'+indexTag},tag.tag);
            })
            /*React.DOM.option({},'prueba'),
            React.DOM.option({},'prueba 2')*/
          )
        ),
        React.createElement(horizontalFab,{
          toggle: false,
          button: {
            color: 'red',
            icon: 'mdi-navigation-menu'
          },
          items: [
            {
              color: 'blue',
              icon: 'add',
              onClick: function(){$('#modal-prueba').openModal(options['configModal']);}
            },
            {
              color: 'brown',
              icon: 'label',
              onClick: function(){$('#modal-tags').openModal(options['configModal']);}
            },
            {
              color: 'red',
              icon: 'delete',
              onClick: function(index,idGroup){
                words.getFlux().actions.deleteAllWordsByGroup(idGroup);
                words.getFlux().actions.deleteAllTagsByGroup(idGroup);
                words.getFlux().actions.deleteGroup(index);
              }.bind(undefined,options['indexGroup'],options['group']._id)
            }
          ]
        }),
        React.createElement(table,{
          //groupName: this.state.group.name,
          table: {
            options: {
              className: 'highlight'
            },
            head: options['group'].columns,
            body: options['wordsTable'],
            buttons: {
              edit: {
                text: 'Edit',
                onClick: function(row,rowIndex){
                  // EL map es el de jQuery, por eso esta al reves

                  $('#form-words input').map(function(inputIndex,input){
                    if(Array.isArray(row[inputIndex])){
                      $('#tags-select-modal-prueba').select2().val(row[inputIndex]).trigger('change');
                    }
                    input.value = row[inputIndex];
                  });

                  options['update'] = rowIndex;
                  //console.log(this.configModal);
                  $('#modal-prueba').openModal(options['configModal']);
                }
              },
              delete: {
                text: 'Delete',
                onClick: function(rowIndex){

                  words.getFlux().actions.deteleWord(rowIndex);
                }
              }
            }
          }
        }),
        React.createElement(modal,{
          modalName: 'modal-prueba',
          /*complete: function(){
            alert('cerrando modal');
            options['update'] = false;
            $('#form-words')[0].reset();
          }.bind(this),*/
          fixed: true,
          trigger: {
            /*className: 'modal-trigger waves-effect waves-light btn',
            text: 'Modal'*/
          },
          content: React.DOM.form({id: 'form-words'},
            options['group'].columns.map(function(column,index){
              return (
                React.createElement(input,{
                  div: {
                    className: 'input-field col s12'
                  },
                  input: {
                    id: column,
                    type: 'text',
                    //placeholder: 'username',
                    //className: 'validate'
                  },
                  label: {
                    htmlFor: column,
                    value: column
                  },
                  key: 'input-form-words-'+index
                })
              )
            }),
            React.DOM.div({className: 'input-field col s12'},
              React.DOM.label({},'Tags'),
              React.DOM.select({multiple: true, id: 'tags-select-modal-prueba',className:'browser-default'},
                options['tags'].map(function(tag,indexTag){
                  return React.DOM.option({key: 'tag-select-modal-prueba-'+indexTag},tag.tag);
                })
                /*React.DOM.option({},'prueba'),
                React.DOM.option({},'prueba 2')*/
              )
            )
          ),
          footer: [
            {
              text: 'close',
              close: true,
              onClick: function(){
                $('select').select2().val(null).trigger('change');
                $('#form-words')[0].reset();
              }
            },
            {
              text: 'save and close',
              close: true,
              onClick: this.insertWord
            },
            {
              text: 'save',
              close: false,
              onClick: this.insertWord
            }
          ]
        }),
        // MODAL DE LAS TAGS
        React.createElement(modal,{
          modalName: 'modal-tags',
          /*complete: function(){
            alert('cerrando modal');
            options['update'] = false;
            $('#form-words')[0].reset();
          }.bind(this),*/
          fixed: true,
          trigger: {
            /*className: 'modal-trigger waves-effect waves-light btn',
            text: 'Modal'*/
          },
          content: React.DOM.form({id: 'form-tags', key: 'form-tags-key'},
            React.createElement(input,{
              div: {
                className: 'input-field col s12'
              },
              input: {
                id: 'input-tag',
                type: 'text',
                //placeholder: 'username',
                //className: 'validate'
              },
              label: {
                htmlFor: 'tag',
                value: 'tag'
              },
              key: 'input-form-tags'
            }),
            React.DOM.div({},
              React.DOM.h4({},'Tags in this group'),
              options['tags'].map(function(tag,tagIndex){
                return (
                  React.DOM.div(
                    {
                      key:'div-tag-form'+tagIndex,
                      className: 'chip',
                      /*onClick: function(){
                        //console.log('la tag index es: '+tag._id);
                        //console.log('el texto de la tag es: '+ tag.tag);
                        words.getFlux().actions.removeTag({id: tag._id,index: tagIndex});
                      }*/
                    },
                    tag.tag,
                    //React.DOM.i({className: '',onClick: function(){console.log('SEEEEEEEEEEEEEEEEE')}},' X ')
                    React.DOM.i({
                      className: '',
                      key: 'icon-tag-close-'+tagIndex,
                      onClick: function(){
                        //console.log('la tag index es: '+tag._id);
                        //console.log('el texto de la tag es: '+ tag.tag);
                        words.getFlux().actions.deleteTagWord({tag: tag.tag,idGroup: options['group']._id});
                        words.getFlux().actions.removeTag({id: tag._id,index: tagIndex});
                      }
                    },' X ')
                  )
                )
              })
            )),
          footer: [
            {
              text: 'close',
              close: true,
              onClick: function(){
                //$('select').select2().val(null).trigger('change');
                //$('#form-words')[0].reset();
                $('#input-tag').val('');
              }
            },
            {
              text: 'save and close',
              close: true,
              onClick: this.insertTag
            },
            {
              text: 'save',
              close: false,
              onClick: this.insertTag
            }
          ]
        })
      )
    )
  }
});
